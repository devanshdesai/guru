﻿using BeautyBook.Common;
using System.Web.Mvc;

namespace Gurupeedam.Controllers
{
    public class ContactController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public JsonResult ContactUsSend(string SuccessMessage = "",string Con_Subject = "", string con_Fname = "", string col_Lname = "", string email = "", string number = "", string write_message = "")
        {
            EmailHelper.SendEmail(Con_Subject, con_Fname, col_Lname, email, number, write_message);
            EmailHelper.UserSendEmail(email , Con_Subject , SuccessMessage , con_Fname, col_Lname);
            return Json(200, JsonRequestBehavior.AllowGet);
        }

    }
}