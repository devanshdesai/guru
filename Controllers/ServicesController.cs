﻿using System.Web.Mvc;

namespace Gurupeedam.Controllers
{
    public class ServicesController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult HandToHand()
        {
            return View();
        }

        public ActionResult GuidanceSessionForHumanity()
        {
            return View();
        }

        public ActionResult CommunityService()
        {
            return View();
        }
    }
}