﻿using BeautyBook.Common;
using System.Web.Mvc;

namespace Gurupeedam.Controllers
{
    public class VolunteerController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public JsonResult VolunteerSend(string volunteering_fname = "", string volunteering_lname = "", string volunteering_email = "", string volunteering_number = "", string volunteering_address = "", string volunteering_Select = "" , string volunteering_feedback = "")
        {
            EmailHelper.SendVolunteer(volunteering_fname, volunteering_lname, volunteering_email, volunteering_number, volunteering_address, volunteering_Select , volunteering_feedback);
            EmailHelper.UserSendVolunteer(volunteering_email , volunteering_fname, volunteering_lname);
            return Json(200, JsonRequestBehavior.AllowGet);
        }

    }
}