﻿using BeautyBook.Common;
using System;
using System.Globalization;
using System.Threading;
using System.Web;
using System.Web.Mvc;

namespace Gurupeedam.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Change(String LanguageAbbrevation)
        {
            if (LanguageAbbrevation != null)
            {
                Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(LanguageAbbrevation);
                Thread.CurrentThread.CurrentUICulture = new CultureInfo(LanguageAbbrevation);
            }

            HttpCookie cookie = new HttpCookie("Language");
            cookie.Value = LanguageAbbrevation;
            Response.Cookies.Add(cookie);

            return View("Index");
        }

        public JsonResult JoinOurNewsletterSend(string FirstName = "", string LastName = "", string Email = "")
        {
            EmailHelper.JoinOurNewsletterSignUp(FirstName, LastName, Email);
            EmailHelper.UserJoinOurNewsletterSignUp(Email);
            return Json(200, JsonRequestBehavior.AllowGet);
        }

        public JsonResult InformationSend(string Information_Gender = "", string Information_Age = "", string Information_Ethnicity = "")
        {
            EmailHelper.InformationSend(Information_Gender, Information_Age, Information_Ethnicity);
            return Json(200, JsonRequestBehavior.AllowGet);
        }

    }
}