﻿using BeautyBook.Common;
using System.Web.Mvc;

namespace Gurupeedam.Controllers
{
    public class ProgramsController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult MindOpeningSessions()
        {
            return View();
        }

        public ActionResult Seminars()
        {
            return View();
        }

        public ActionResult SummerWinterCamps()
        {
            return View();
        }

        public ActionResult HeartToHeartCommunication()
        {
            return View();
        }

        public ActionResult AnimalCare()
        {
            return View();
        }


    }
}