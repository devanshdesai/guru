﻿using BeautyBook.Common;
using Newtonsoft.Json;
using System;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.Mvc;

namespace Gurupeedam.Controllers
{
    public class GalleryController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }


        public JsonResult GetImages()
        {
            ViewBag.Images = Directory.EnumerateFiles(Server.MapPath("/Content/asset/img/GalleryImage/")).Select(fn => "/Content/asset/img/GalleryImage/" + Path.GetFileName(fn));
            return Json(ViewBag.Images, JsonRequestBehavior.AllowGet);
        }
    }
}