﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Gurupeedam.Resources {
    using System;
    
    
    /// <summary>
    ///   A strongly-typed resource class, for looking up localized strings, etc.
    /// </summary>
    // This class was auto-generated by the StronglyTypedResourceBuilder
    // class via a tool like ResGen or Visual Studio.
    // To add or remove a member, edit your .ResX file then rerun ResGen
    // with the /str option, or rebuild your VS project.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "16.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    public class LanguagePrograms {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal LanguagePrograms() {
        }
        
        /// <summary>
        ///   Returns the cached ResourceManager instance used by this class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("Gurupeedam.Resources.LanguagePrograms", typeof(LanguagePrograms).Assembly);
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Overrides the current thread's CurrentUICulture property for all
        ///   resource lookups using this strongly typed resource class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Animal Care.
        /// </summary>
        public static string Animal_Care {
            get {
                return ResourceManager.GetString("Animal Care", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to The Animal Care Program is designed to allow individuals to experience the beauty and purity of nature and to get in touch with their inner feelings - it provides an opportunity for those who do not have the facility to go close to animals to come to our facility and spend time and take care of the animals..
        /// </summary>
        public static string Animal_Care_Content_1 {
            get {
                return ResourceManager.GetString("Animal Care Content 1", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to At Gurupeedam, we love and take the utmost care of our beloved animal family. Our animal family currently includes:.
        /// </summary>
        public static string Animal_Care_Content_2 {
            get {
                return ResourceManager.GetString("Animal Care Content 2", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Individuals are provided the opportunity to identify their self confidence by participating in various activities that are important in taking care of the animals - from building a ranch or chicken coop, feeding the animals, bathing the animals, talking and playing with animals and cleaning their living areas and being a part of the birthing process, etc..
        /// </summary>
        public static string Animal_Care_Content_3 {
            get {
                return ResourceManager.GetString("Animal Care Content 3", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to The three horses we had at Gurupeedam are currently being homed in a different location as they need a larger facility.  We are looking forward to making necessary arrangements soon to be able to bring horses back to our property..
        /// </summary>
        public static string Animal_Care_Content_4 {
            get {
                return ResourceManager.GetString("Animal Care Content 4", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Gurupeedam provides the opportunity and facility for individuals to put their interest and build their values.  Many of the volunteers or service lovers who take care of our animals right now never had any prior experience of working with animals but their involvement and participation has grown their interest and helped build their self confidence..
        /// </summary>
        public static string Animal_Care_Content_5 {
            get {
                return ResourceManager.GetString("Animal Care Content 5", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Kalyani (mother cow).
        /// </summary>
        public static string Animal_Care_List_1 {
            get {
                return ResourceManager.GetString("Animal Care List 1", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Thirapauthy (mother cow).
        /// </summary>
        public static string Animal_Care_List_2 {
            get {
                return ResourceManager.GetString("Animal Care List 2", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Arjun (Kalyani’s baby male calf).
        /// </summary>
        public static string Animal_Care_List_3 {
            get {
                return ResourceManager.GetString("Animal Care List 3", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Rakshitha (Thirapauthy’s baby female calf).
        /// </summary>
        public static string Animal_Care_List_4 {
            get {
                return ResourceManager.GetString("Animal Care List 4", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Chella (male cow).
        /// </summary>
        public static string Animal_Care_List_5 {
            get {
                return ResourceManager.GetString("Animal Care List 5", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Chickens.
        /// </summary>
        public static string Animal_Care_List_6 {
            get {
                return ResourceManager.GetString("Animal Care List 6", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Baby chicks.
        /// </summary>
        public static string Animal_Care_List_7 {
            get {
                return ResourceManager.GetString("Animal Care List 7", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Roosters.
        /// </summary>
        public static string Animal_Care_List_8 {
            get {
                return ResourceManager.GetString("Animal Care List 8", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Canada.
        /// </summary>
        public static string Canada {
            get {
                return ResourceManager.GetString("Canada", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Heart to Heart Communication.
        /// </summary>
        public static string Heart_to_Heart_Communication {
            get {
                return ResourceManager.GetString("Heart to Heart Communication", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to The Heart to Heart Communication Program focuses predominantly on Experiencable Valuable Individuals (EVIs), which is what we love to call the seniors that come to Gurupeedam. The intent of this program is to support EVIs to see their own value through their life experiences and build self-confidence.  We enable EVIs to identify opportunities to openly communicate from heart to heart to make them feel loved and cared for.  Through our love, we provide care and ensure each participant is treated with kindnes [rest of string was truncated]&quot;;.
        /// </summary>
        public static string Heart_to_Heart_Communication_Content_1 {
            get {
                return ResourceManager.GetString("Heart to Heart Communication Content 1", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Many seniors see themselves through their understanding of good and bad/right and wrong rather than through their life’s experiences.  This becomes the cause for their struggles and suffering and, without realizing, they lose their self-confidence.  At Gurupeedam, we provide opportunities for every senior to become EVIs by encouraging them to free from excuses and actively participate in programs targetted to make them feel calmness, joy and self satisfaction..
        /// </summary>
        public static string Heart_to_Heart_Communication_Content_2 {
            get {
                return ResourceManager.GetString("Heart to Heart Communication Content 2", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Another part of the Heart to Heart Program is working with expectant mothers and providing them an opportunity to experience the great feeling of their child before birth. The care the mother provides to her child while in the womb is essential to the child’s development. At Gurupeedam, we provide services to expectant mothers for them to experience the value of pregnancy and beauty of motherhood. We celebrate and welcome the upcoming birth of the child..
        /// </summary>
        public static string Heart_to_Heart_Communication_Content_3 {
            get {
                return ResourceManager.GetString("Heart to Heart Communication Content 3", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to At Gurupeedam, we also work with children through a series of activities to empower them to realize their values through the love and care that their parents provide. We work with parents and their children together to guide their children through various heart-to-heart sessions. Through this program children will have an opportunity to realize their own values and abilities as well building their self discipline and grow to be self confident individuals..
        /// </summary>
        public static string Heart_to_Heart_Communication_Content_4 {
            get {
                return ResourceManager.GetString("Heart to Heart Communication Content 4", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to &lt;?xml version=&quot;1.0&quot; encoding=&quot;utf-8&quot;?&gt;
        ///&lt;root&gt;
        ///  &lt;!-- 
        ///    Microsoft ResX Schema 
        ///    
        ///    Version 2.0
        ///    
        ///    The primary goals of this format is to allow a simple XML format 
        ///    that is mostly human readable. The generation and parsing of the 
        ///    various data types are done through the TypeConverter classes 
        ///    associated with the data types.
        ///    
        ///    Example:
        ///    
        ///    ... ado.net/XML headers &amp; schema ...
        ///    &lt;resheader name=&quot;resmimetype&quot;&gt;text/microsoft-resx&lt;/resheader&gt;
        ///    &lt;resheader n [rest of string was truncated]&quot;;.
        /// </summary>
        public static string LanguageHeader {
            get {
                return ResourceManager.GetString("LanguageHeader", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to //------------------------------------------------------------------------------
        ///// &lt;auto-generated&gt;
        /////     This code was generated by a tool.
        /////     Runtime Version:4.0.30319.42000
        /////
        /////     Changes to this file may cause incorrect behavior and will be lost if
        /////     the code is regenerated.
        ///// &lt;/auto-generated&gt;
        /////------------------------------------------------------------------------------
        ///
        ///namespace Gurupeedam.Resources {
        ///    using System;
        ///    
        ///    
        ///    /// &lt;summary&gt;
        ///    ///   A strongly- [rest of string was truncated]&quot;;.
        /// </summary>
        public static string LanguageHeader_Designer {
            get {
                return ResourceManager.GetString("LanguageHeader_Designer", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Under the guidance of our Guru, Gurupeedam is providing the following programs to those who seek guidance to self-realize and build their self confidence..
        /// </summary>
        public static string Main_Content {
            get {
                return ResourceManager.GetString("Main Content", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Mind Realization Session.
        /// </summary>
        public static string Mind_Realization_Session {
            get {
                return ResourceManager.GetString("Mind Realization Session", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Mind Realization Sessions are provided in group settings to allow individuals to express themselves freely and openly to provide an opportunity for individuals to realize about themselves and come out of what may be a blockage in their lives. The power of openness will be experienced by the individuals attending these sessions - a sense of clarity and self-awareness will arise from these sessions that will be great for everyone to feel..
        /// </summary>
        public static string Mind_Realization_Session_Content_1 {
            get {
                return ResourceManager.GetString("Mind Realization Session Content 1", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Please click on each of the programs below for more information..
        /// </summary>
        public static string Please_Click {
            get {
                return ResourceManager.GetString("Please Click", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Programs.
        /// </summary>
        public static string Programs {
            get {
                return ResourceManager.GetString("Programs", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Gurupeedam is expanding our programs and services globally - please stay tuned for more updates!.
        /// </summary>
        public static string Programs_Note {
            get {
                return ResourceManager.GetString("Programs Note", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Request Submitted Succssfully.
        /// </summary>
        public static string Request_Submitted_Succssfully {
            get {
                return ResourceManager.GetString("Request Submitted Succssfully", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Seminars.
        /// </summary>
        public static string Seminars {
            get {
                return ResourceManager.GetString("Seminars", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Seminars or Classes are provided on various topics based on request or need of the individuals interested. Sessions provide opportunities to gain valuable guidance from our Guru to put into practice in our day to day life to build self confidence - ranging from yoga, meditation, deep breathing exercises to management, finances and many more..
        /// </summary>
        public static string Seminars___Classes_Content_1 {
            get {
                return ResourceManager.GetString("Seminars & Classes Content 1", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Summer &amp; Winter Camps.
        /// </summary>
        public static string Summer___Winter_Camps {
            get {
                return ResourceManager.GetString("Summer & Winter Camps", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Gurupeedam organizes summer and winter camps to provide an opportunity for individuals of all ages to participate in various activities. The Camps provide sessions over the course of a few days and enable the participants to obtain Guru’s guidance and take part in outdoor activities depending on the season.  Activities may include yoga practices, spending time with nature, hiking on trails, etc..
        /// </summary>
        public static string Summer___Winter_Camps_Content_1 {
            get {
                return ResourceManager.GetString("Summer & Winter Camps Content 1", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to UK.
        /// </summary>
        public static string UK {
            get {
                return ResourceManager.GetString("UK", resourceCulture);
            }
        }
    }
}
