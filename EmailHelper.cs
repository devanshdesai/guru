﻿namespace BeautyBook.Common
{
    using HelpmatePublic;
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Net;
    using System.Net.Mail;
    using System.Text;
    using System.Text.RegularExpressions;
    using System.Threading.Tasks;
    using System.Web.Hosting;
    using System.Web.Script.Serialization;
    //using Twilio;
    //using Twilio.Rest.Api.V2010.Account;

    public class EmailHelper
    {
        /// <summary>
        /// Sending An Email with master mail template
        /// </summary>
        /// <param name="mailFrom">Mail From</param>
        /// <param name="mailTo">Mail To</param>
        /// <param name="mailCC">Mail CC</param>
        /// <param name="mailBCC">Mail BCC</param>
        /// <param name="subject">Subject of mail</param>
        /// <param name="body">Body of mail</param>
        /// <param name="attachment">Attachment for the mail</param>
        /// <param name="emailType">Email Type</param>
        /// <returns>return send status</returns>


        public static void SendPushNotification(string Id, string bodyt, string titlet)
        {
            string response;
            try
            {
                if (string.IsNullOrEmpty(Id) == true)
                {
                    throw new Exception("");
                }
                string serverKey = Configurations.ServerKey;
                string senderId = Configurations.SenderId;
                string deviceId = Id;

                WebRequest tRequest = WebRequest.Create("https://fcm.googleapis.com/fcm/send");

                tRequest.Method = "post";
                tRequest.ContentType = "application/json";
                var data = new
                {
                    to = deviceId,
                    notification = new
                    {
                        body = bodyt,//"Your job Status has been changed",
                        title = titlet,//"Fitting job has been completed.",
                        sound = "Enabled"
                    }
                };

                var serializer = new JavaScriptSerializer();
                var json = serializer.Serialize(data);
                Byte[] byteArray = Encoding.UTF8.GetBytes(json);
                tRequest.Headers.Add(string.Format("Authorization: key={0}", serverKey));
                tRequest.Headers.Add(string.Format("Sender: id={0}", senderId));
                tRequest.ContentLength = byteArray.Length;

                using (Stream dataStream = tRequest.GetRequestStream())
                {
                    dataStream.Write(byteArray, 0, byteArray.Length);
                    using (WebResponse tResponse = tRequest.GetResponse())
                    {
                        using (Stream dataStreamResponse = tResponse.GetResponseStream())
                        {
                            using (StreamReader tReader = new StreamReader(dataStreamResponse))
                            {
                                String sResponseFromServer = tReader.ReadToEnd();
                                response = sResponseFromServer;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                response = ex.Message;
            }

            //if (moblie != null && moblie != string.Empty)
            //{
            //    SendMsgViaTwilio(titlet + " - " + bodyt, moblie);
            //}

            //AdminSend("bereket@auroratechafrica.com", "", "", titlet, bodyt);
        }

        //Send contact us email send
        public static bool SendEmail(string Con_Subject, string con_Fname, string col_Lname, string email, string number, string write_message)
        {
            //demo
            string body = "";
            using (StreamReader reader = new StreamReader(HostingEnvironment.MapPath("~/EmailTemplate/EmailTemplateForContactUs.html")))
            {
                body = reader.ReadToEnd();
            }

            body = body.Replace("@welcometext", "New Inquiry.");

            body = body.Replace("@Con_Fname", con_Fname);
            body = body.Replace("@Col_Lname", col_Lname);
            body = body.Replace("@Con_email", email);
            body = body.Replace("@Con_number", number);
            body = body.Replace("@Con_write_message", write_message);
            bool reason = EmailHelper.Send("info@gurupeedam.com", "", "", Con_Subject + " - " + con_Fname + " " + col_Lname + " (" + (DateTime.Today.ToString("MMMM dd, yyyy")) + ")", body);
            return reason;
        }

        public static bool UserSendEmail(string email, string Con_Subject, string SuccessMessage, string con_Fname, string col_Lname)
        {
            //demo
            string body = "";
            using (StreamReader reader = new StreamReader(HostingEnvironment.MapPath("~/EmailTemplate/UserSuccessMessage.html")))
            {
                body = reader.ReadToEnd();
            }

            body = body.Replace("@welcomeIcon", "https://cdn-icons-png.flaticon.com/512/1202/1202009.png");

            body = body.Replace("@SuccessMessage", SuccessMessage);
            bool reason = EmailHelper.Send(email, "", "", Con_Subject, body);
            return reason;
        }

        //JoinOurNewsletterSignUp
        public static bool JoinOurNewsletterSignUp(string FirstName, string LastName, string Email)
        {
            //demo
            string body = "";
            using (StreamReader reader = new StreamReader(HostingEnvironment.MapPath("~/EmailTemplate/JoinOurNewsletterTemplate.html")))
            {
                body = reader.ReadToEnd();
            }

            body = body.Replace("@welcometext", "New SignUp");

            body = body.Replace("@Firstname", FirstName);
            body = body.Replace("@Lastname", LastName);
            body = body.Replace("@Email", Email);
            bool reason = EmailHelper.Send("info@gurupeedam.com", "", "", "Join Our Newsletter" + " - " + FirstName + " " + LastName + " (" + (DateTime.Today.ToString("MMMM dd, yyyy")) + ")", body);
            return reason;
        }

        public static bool UserJoinOurNewsletterSignUp(string email)
        {
            //demo
            string body = "";
            using (StreamReader reader = new StreamReader(HostingEnvironment.MapPath("~/EmailTemplate/UserSuccessMessage.html")))
            {
                body = reader.ReadToEnd();
            }

            body = body.Replace("@welcomeIcon", "https://cdn-icons-png.flaticon.com/512/1202/1202009.png");

            body = body.Replace("@SuccessMessage", "Thank you for signing up for the newsletter.");
            bool reason = EmailHelper.Send(email, "", "", "Join Our Newsletter", body);
            return reason;
        }

        //information
        public static bool InformationSend(string Information_Gender, string Information_Age, string Information_Ethnicity)
        {
            //demo
            string body = "";
            using (StreamReader reader = new StreamReader(HostingEnvironment.MapPath("~/EmailTemplate/InformationTemplate.html")))
            {
                body = reader.ReadToEnd();
            }

            body = body.Replace("@welcometext", "Visitor Information");

            body = body.Replace("@information_Gender", Information_Gender);
            body = body.Replace("@information_Age", Information_Age);
            body = body.Replace("@information_Ethnicity", Information_Ethnicity);
            bool reason = EmailHelper.Send("gurupeedam.info@gmail.com", "", "", "Visitor Information" + " (" + (DateTime.Today.ToString("MMMM dd, yyyy")) + ")", body);
            return reason;
        }

        //information
        public static bool Donate(string DonateAmount, string ReceiptEmail, string DonationStatus, string TransationDate)
        {
            //demo
            string body = "";
            using (StreamReader reader = new StreamReader(HostingEnvironment.MapPath("~/EmailTemplate/DonationTemplate.html")))
            {
                body = reader.ReadToEnd();
            }

            body = body.Replace("@welcometext", "Donation Successfully");

            body = body.Replace("@Donation_Email", Convert.ToString(ReceiptEmail));
            body = body.Replace("@Donation_Status", Convert.ToString(DonationStatus));
            body = body.Replace("@Donation_TransationDate", Convert.ToString(TransationDate));
            body = body.Replace("@Donation_Amount", DonateAmount);
            bool reason = EmailHelper.Send("kartik.soboft@gmail.com", "", "", "Donation" + " (" + (DateTime.Today.ToString("MMMM dd, yyyy")) + ")", body);
            return reason;
        }

        //VoulnteerSend
        public static bool SendVolunteer(string volunteering_fname, string volunteering_lname, string volunteering_email, string volunteering_number, string volunteering_address, string volunteering_Select, string volunteering_feedback)
        {
            //demo
            string body = "";
            using (StreamReader reader = new StreamReader(HostingEnvironment.MapPath("~/EmailTemplate/EmailTemplateForVoulnteer.html")))
            {
                body = reader.ReadToEnd();
            }

            var SubjectName = "Volunteering";

            body = body.Replace("@welcometext", "New SignUp");

            body = body.Replace("@Volunteering_fname", volunteering_fname);
            body = body.Replace("@Volunteering_lname", volunteering_lname);
            body = body.Replace("@Volunteering_email", volunteering_email);
            body = body.Replace("@Volunteering_number", volunteering_number);
            body = body.Replace("@Volunteering_address", volunteering_address);
            body = body.Replace("@Volunteering_Select", volunteering_Select);
            body = body.Replace("@Volunteering_feedback", volunteering_feedback);
            bool reason = EmailHelper.Send("info@gurupeedam.com", "", "", SubjectName + " - " + volunteering_fname + " " + volunteering_lname + " (" + (DateTime.Today.ToString("MMMM dd, yyyy")) + ")", body);
            return reason;
        }

        public static bool UserSendVolunteer(string volunteering_email, string volunteering_fname, string volunteering_lname)
        {

            var SubjectName = "Volunteering";

            //demo
            string body = "";
            using (StreamReader reader = new StreamReader(HostingEnvironment.MapPath("~/EmailTemplate/UserSuccessMessage.html")))
            {
                body = reader.ReadToEnd();
            }

            body = body.Replace("@welcomeIcon", "https://cdn-icons-png.flaticon.com/512/1202/1202009.png");

            body = body.Replace("@SuccessMessage", "Thank you for your interest in volunteering at Gurupeedam. We value your interest and will get back to you shortly.");
            bool reason = EmailHelper.Send(volunteering_email, "", "", SubjectName, body);
            return reason;
        }


        ////Send quote email send
        //public static bool SendQuote(string Subject, string quote_fullname, string quote_Email, string quote_Phone, string quote_Timeline, string quote_Budget, string quote_Describe)
        //{
        //    //demo
        //    string body = "";
        //    using (StreamReader reader = new StreamReader(HostingEnvironment.MapPath("~/EmailTemplate/EmailTemplateForQuote.html")))
        //    {
        //        body = reader.ReadToEnd();
        //    }

        //    body = body.Replace("@welcometext", "New Quote Inquiry.");

        //    body = body.Replace("@Quote_fullname", quote_fullname);
        //    body = body.Replace("@Quote_Email", quote_Email);
        //    body = body.Replace("@Quote_Phone", quote_Phone);
        //    body = body.Replace("@Quote_Timeline", quote_Timeline);
        //    body = body.Replace("@Quote_Budget", quote_Budget);
        //    body = body.Replace("@Quote_Describe", quote_Describe);
        //    bool reason = EmailHelper.Send(quote_Email, "", "", Subject, body);
        //    return reason;
        //}


        //Send quote email send
        public static bool DonatedUser(string OrganizationNameOrIndividualName,
            string OrganizationOrIndividualEmail,
            string Phone,
            string City,
            string AddressLine,
            string State,
            string Note,
            string stripeCharge)
        {
            //demo
            string body = "";
            using (StreamReader reader = new StreamReader(HostingEnvironment.MapPath("~/EmailTemplate/DonatedUsersTemplate.html")))
            {
                body = reader.ReadToEnd();
            }

            body = body.Replace("@welcometext", "New Quote Inquiry.");

            body = body.Replace("@OrganizationNameOrIndividualName", OrganizationNameOrIndividualName);
            body = body.Replace("@OrganizationOrIndividualEmail", OrganizationOrIndividualEmail);
            body = body.Replace("@Phone", Phone);
            body = body.Replace("@AddressLine", AddressLine);
            body = body.Replace("@City", City);
            body = body.Replace("@State", State);
            body = body.Replace("@Note", Note);
            body = body.Replace("@stripeCharge", stripeCharge);

            //info@gurupeedam.com
            bool reason = EmailHelper.Send("info@gurupeedam.com", "", "", "Online donation" + " - " + OrganizationNameOrIndividualName + " (" + (DateTime.Today.ToString("MMMM dd, yyyy")) + ")", body);
            return reason;
        }

        public static bool Send(string mailTo, string mailCC, string mailBCC, string subject, string body, List<byte[]> attachmentFile = null, List<string> attachmentName = null)
        {
            Boolean issent = true;
            string mailFrom;
            mailFrom = Configurations.FromEmailAddress;
            mailBCC = Configurations.BccEmailAddress;
            try
            {
                if (!string.IsNullOrWhiteSpace(mailFrom) && !string.IsNullOrWhiteSpace(mailTo))
                {
                    MailMessage mailMesg = new MailMessage();
                    SmtpClient objSMTP = new SmtpClient();

                    objSMTP.Host = Configurations.EmailHost;

                    objSMTP.EnableSsl = Convert.ToBoolean(Configurations.EnableSsl);

                    System.Net.NetworkCredential NetworkCred = new System.Net.NetworkCredential();

                    NetworkCred.UserName = Configurations.EmailUserName; //reading from web.config  

                    NetworkCred.Password = Convert.ToString(ConvertTo.Base64Decode(Configurations.EmailPassword)); //reading from web.config  

                    objSMTP.UseDefaultCredentials = true;

                    objSMTP.Credentials = new System.Net.NetworkCredential(NetworkCred.UserName, NetworkCred.Password);

                    objSMTP.Port = int.Parse(Configurations.Port);

                    mailMesg.From = new System.Net.Mail.MailAddress(mailFrom);
                    mailMesg.To.Add(mailTo);

                    if (!string.IsNullOrEmpty(mailCC))
                    {
                        string[] mailCCArray = mailCC.Split(';');
                        foreach (string email in mailCCArray)
                        {
                            mailMesg.CC.Add(email);
                        }
                    }

                    if (!string.IsNullOrEmpty(mailBCC))
                    {
                        mailBCC = mailBCC.Replace(";", ",");
                        mailMesg.Bcc.Add(mailBCC);
                    }

                    if (attachmentFile != null && attachmentName != null)
                    {
                        byte[][] Files = attachmentFile.ToArray();
                        string[] Names = attachmentName.ToArray();
                        if (Files != null)
                        {
                            for (int i = 0; i < Files.Length; i++)
                            {
                                mailMesg.Attachments.Add(new Attachment(new MemoryStream(Files[i]), Names[i]));
                            }
                        }
                    }

                    mailMesg.Subject = subject;
                    mailMesg.Body = body;
                    mailMesg.IsBodyHtml = true;

                    try
                    {
                        objSMTP.Send(mailMesg);
                        issent = true;
                        return issent;
                    }
                    catch (Exception ex)
                    {
                        mailMesg.Dispose();
                        //mailMesg = null;
                        //CommonService.Log(e);
                        issent = false;
                        return issent;
                    }
                    finally
                    {
                        mailMesg.Dispose();
                    }
                }
                else
                {
                    return false;
                }
            }
            catch
            {
                return false;
            }

        }


        /// <summary>
        /// Method is used to Validate Email
        /// </summary>
        /// <param name="fromEmail">From email List</param>
        /// <param name="toEmail">To Email list</param>
        /// <returns>Returns validation result</returns>
        private static bool ValidateEmail(string fromEmail, string toEmail)
        {
            bool isValid = true;
            if (!IsEmail(fromEmail))
            {
                isValid = false;
            }

            if (!string.IsNullOrEmpty(toEmail))
            {
                toEmail = toEmail.Replace(" ", string.Empty);
                string[] emailList = null;
                try
                {
                    emailList = toEmail.Split(',');
                }
                catch
                {
                    isValid = false;
                }

                if (emailList != null && emailList.Count() > 0)
                {
                    foreach (string email in emailList)
                    {
                        if (!IsEmail(email))
                        {
                            isValid = false;
                        }
                    }
                }
                else
                {
                    isValid = false;
                }
            }
            else
            {
                isValid = false;
            }

            return isValid;
        }

        /// <summary>
        /// Check email string is Email or not
        /// </summary>
        /// <param name="email">Email to verify</param>
        /// <returns>return email validation result</returns>
        private static bool IsEmail(string email)
        {
            string strRegex = @"^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}" +
                  @"\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\" +
                  @".)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$";
            Regex re = new Regex(strRegex);
            if (re.IsMatch(email))
            {
                return true;
            }
            else
            {
                return false;
            }
        }


        //public static string SendMsgViaTwilio(string msgBody, string mobileno)
        //{
        //    string status = "FAIL";
        //    try
        //    {
        //        string accountSid = Configurations.TwilioAccountSid;
        //        string authToken = Configurations.TwilioAuthToken;
        //        TwilioClient.Init(accountSid, authToken);

        //        var message = MessageResource.Create(
        //          body: msgBody,     // replace with Body
        //          to: new Twilio.Types.PhoneNumber(mobileno),   // replace with customer number 
        //          from: new Twilio.Types.PhoneNumber(Configurations.FromMobileNumber) // +13254201234 //
        //      );
        //        status = "SUCCESS";
        //    }
        //    catch (Exception ex)
        //    {
        //        status = "FAIL" + ex.Message;
        //    }

        //    return status;
        //}
    }
}
