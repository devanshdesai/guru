﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Gurupeedam.Resources {
    using System;
    
    
    /// <summary>
    ///   A strongly-typed resource class, for looking up localized strings, etc.
    /// </summary>
    // This class was auto-generated by the StronglyTypedResourceBuilder
    // class via a tool like ResGen or Visual Studio.
    // To add or remove a member, edit your .ResX file then rerun ResGen
    // with the /str option, or rebuild your VS project.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "16.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    public class LanguageHeader {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal LanguageHeader() {
        }
        
        /// <summary>
        ///   Returns the cached ResourceManager instance used by this class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("Gurupeedam.Resources.LanguageHeader", typeof(LanguageHeader).Assembly);
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Overrides the current thread's CurrentUICulture property for all
        ///   resource lookups using this strongly typed resource class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to About Us.
        /// </summary>
        public static string About_Us {
            get {
                return ResourceManager.GetString("About Us", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Above.
        /// </summary>
        public static string Above {
            get {
                return ResourceManager.GetString("Above", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to American Indian or Alaska Native.
        /// </summary>
        public static string American_Indian_or_Alaska_Native {
            get {
                return ResourceManager.GetString("American Indian or Alaska Native", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Animal Care.
        /// </summary>
        public static string Animal_Care {
            get {
                return ResourceManager.GetString("Animal Care", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Asian.
        /// </summary>
        public static string Asian {
            get {
                return ResourceManager.GetString("Asian", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Black or African American.
        /// </summary>
        public static string Black_or_African_American {
            get {
                return ResourceManager.GetString("Black or African American", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Community Service.
        /// </summary>
        public static string Community_Service {
            get {
                return ResourceManager.GetString("Community Service", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Contact Us.
        /// </summary>
        public static string Contact_Us {
            get {
                return ResourceManager.GetString("Contact Us", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to donate.
        /// </summary>
        public static string donate {
            get {
                return ResourceManager.GetString("donate", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Female.
        /// </summary>
        public static string Female {
            get {
                return ResourceManager.GetString("Female", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Gallery.
        /// </summary>
        public static string Gallery {
            get {
                return ResourceManager.GetString("Gallery", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Guidance Session for Humanity.
        /// </summary>
        public static string Guidance_Session_for_Humanity {
            get {
                return ResourceManager.GetString("Guidance Session for Humanity", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Guru.
        /// </summary>
        public static string Guru {
            get {
                return ResourceManager.GetString("Guru", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Hand to Hand.
        /// </summary>
        public static string Hand_to_Hand {
            get {
                return ResourceManager.GetString("Hand to Hand", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Heart to Heart Communication.
        /// </summary>
        public static string Heart_to_Heart_Communication {
            get {
                return ResourceManager.GetString("Heart to Heart Communication", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Hispanic or Latino.
        /// </summary>
        public static string Hispanic_or_Latino {
            get {
                return ResourceManager.GetString("Hispanic or Latino", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Home.
        /// </summary>
        public static string Home {
            get {
                return ResourceManager.GetString("Home", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Please fill in the below information as it will help us to serve the community and individuals better by creating the appropriate programs and services. Thank you!.
        /// </summary>
        public static string information_Heading {
            get {
                return ResourceManager.GetString("information Heading", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to .
        /// </summary>
        public static string information_modal_content_start {
            get {
                return ResourceManager.GetString("information modal content start", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Male.
        /// </summary>
        public static string Male {
            get {
                return ResourceManager.GetString("Male", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Mind Realization Session.
        /// </summary>
        public static string Mind_Realization_Session {
            get {
                return ResourceManager.GetString("Mind Realization Session", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Native Hawaiian or Other Pacific Islander.
        /// </summary>
        public static string Native_Hawaiian_or_Other_Pacific_Islander {
            get {
                return ResourceManager.GetString("Native Hawaiian or Other Pacific Islander", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Other.
        /// </summary>
        public static string Other {
            get {
                return ResourceManager.GetString("Other", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Programs.
        /// </summary>
        public static string Programs {
            get {
                return ResourceManager.GetString("Programs", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Select Ethnicity.
        /// </summary>
        public static string Select_Ethnicity {
            get {
                return ResourceManager.GetString("Select Ethnicity", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Select Gender.
        /// </summary>
        public static string Select_Gender {
            get {
                return ResourceManager.GetString("Select Gender", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Seminars.
        /// </summary>
        public static string Seminars {
            get {
                return ResourceManager.GetString("Seminars", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Services.
        /// </summary>
        public static string Services {
            get {
                return ResourceManager.GetString("Services", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Skip.
        /// </summary>
        public static string Skip {
            get {
                return ResourceManager.GetString("Skip", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Other.
        /// </summary>
        public static string String1 {
            get {
                return ResourceManager.GetString("String1", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Summer &amp; Winter Camps.
        /// </summary>
        public static string Summer_Winter_Camps {
            get {
                return ResourceManager.GetString("Summer Winter Camps", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Under.
        /// </summary>
        public static string Under {
            get {
                return ResourceManager.GetString("Under", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Volunteer.
        /// </summary>
        public static string Volunteer {
            get {
                return ResourceManager.GetString("Volunteer", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to White.
        /// </summary>
        public static string White {
            get {
                return ResourceManager.GetString("White", resourceCulture);
            }
        }
    }
}
