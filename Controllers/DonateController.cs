﻿using BeautyBook.Common;
using HelpmatePublic;
using Stripe;
using System;
using System.Data;
using System.Data.SqlClient;
using System.Web.Mvc;

namespace Gurupeedam.Controllers
{
    public class DonateController : Controller
    {
        public ActionResult Index()
        {
            ViewBag.StripePublishKey = Configurations.StripePublishableKey;
            return View();
        }

        public ActionResult TransactionSuccess()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Charge(
                string stripeToken, 
                string stripeEmail, 
                int amountInCents, 
                string DonateAmount,
                string OrganizationNameOrIndividualName,
                string OrganizationOrIndividualEmail,
                string Phone,
                string City,
                string State,
                string Note,
                string AddressLine,
                string amountInDollars
            )
        {
            try
            {
                if (amountInCents > 0)
                {
                    Stripe.StripeConfiguration.SetApiKey(Configurations.StripePublishableKey);
                    Stripe.StripeConfiguration.ApiKey = Configurations.StripeSecretKey;

                    var myCharge = new Stripe.ChargeCreateOptions();
                    // always set these properties
                    myCharge.Amount = amountInCents;
                    myCharge.Currency = "CAD";
                    myCharge.ReceiptEmail = stripeEmail;
                    myCharge.Description = "Donation";
                    myCharge.Source = stripeToken;
                    myCharge.Capture = true;
                    var chargeService = new Stripe.ChargeService();
                    Charge stripeCharge = chargeService.Create(myCharge);

                    //insert transaction details
                    SqlConnection con = new SqlConnection(Configurations.ConnectionString);
                    SqlCommand cmd = new SqlCommand("InsertPaymentTransaction", con);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@OrganizationNameOrIndividualName", OrganizationNameOrIndividualName);
                    cmd.Parameters.AddWithValue("@OrganizationOrIndividualEmail", OrganizationOrIndividualEmail);
                    cmd.Parameters.AddWithValue("@Phone", Phone);
                    cmd.Parameters.AddWithValue("@AddressLine", AddressLine);
                    cmd.Parameters.AddWithValue("@City", City);
                    cmd.Parameters.AddWithValue("@State", State);
                    cmd.Parameters.AddWithValue("@Note", Note);
                    cmd.Parameters.AddWithValue("@Email", stripeCharge.ReceiptEmail);
                    cmd.Parameters.AddWithValue("@Status", stripeCharge.Status);
                    cmd.Parameters.AddWithValue("@TransactionId", stripeCharge.BalanceTransactionId);
                    cmd.Parameters.AddWithValue("@TransactionDate", stripeCharge.Created.ToString());
                    cmd.Parameters.AddWithValue("@Last4 ", stripeCharge.PaymentMethodDetails.Card.Last4);
                    cmd.Parameters.AddWithValue("@ExpMonth", stripeCharge.PaymentMethodDetails.Card.ExpMonth);
                    cmd.Parameters.AddWithValue("@ExpYear", stripeCharge.PaymentMethodDetails.Card.ExpYear);
                    cmd.Parameters.AddWithValue("@Amount", stripeCharge.Amount);
                    con.Open();
                    int i = cmd.ExecuteNonQuery();

                    EmailHelper.Donate(DonateAmount, stripeCharge.ReceiptEmail, stripeCharge.Status , stripeCharge.Created.ToString());
                    EmailHelper.DonatedUser(OrganizationNameOrIndividualName,
                        OrganizationOrIndividualEmail,
                        Phone,
                        AddressLine,
                        City,
                        State,
                        Note,
                        DonateAmount);

                    con.Close();

                    return Json(200, JsonRequestBehavior.AllowGet);
                }
            }
            catch(Exception ex)
            {
                return new HttpStatusCodeResult(403, ex.Message);
            }

            return RedirectToAction("Index");

        }
    }
}